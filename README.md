# Fort 2024 CAD

## Name
EC Build 2024 Fort CAD (Temple of Doom)

## Description
Originally, this was going to be a fort with internal net, ropes course, and top rope climbing wall. After EHS and legal meetings, it became a fort with internal net, no ropes course, and 12ft bouldering wall. Due to the unusual number of design changes made over the course of many months, the CAD is a bit of a mess. If you're looking for an example of good CAD practices, check out previous years.

## IMPORTANT: Branches
[main](https://gitlab.com/ec-build-2024/fort-2024-cad): Our main branch. Contains the fort planned for build in August 2024.
[with_ropes_course](https://gitlab.com/ec-build-2024/fort-2024-cad/-/tree/branch1): Fort with full ropes course, at height, with nets. Extensive load calculations and presentation about net strength were done; only obstacle was MA legal restrictions on ropes courses.
[shorter_fort](https://gitlab.com/ec-build-2024/fort-2024-cad/-/tree/shorter_fort): Fort with shorter ropes course, without nets, 2ft from ground. Same issue as above.

Here are the CAD rules for naming conventions and stuff

# Rule 1
## Do not hide components; if you need to remove something from CAD, suppress it. The auto-generated BOM will include hidden components, creating problems with screw and wood count. To make this easier, try to mate everything to the assembly planes or to parts that will never need to be removed.

# Rule 2
## Use Weldments. The only components using custom extrude should be plywood or non-wood parts, in which case pay close attention to naming conventions (see rule 4).

# Rule 3
## This year we wasted time on the load calculations because that is what they did in previous years but solidworks can handle the vast majority of them including gravity, windloads, and liveloads (the humans). We highly suggest using this method and testing your CAD somewhat frequently for simulation.

# Rule 4
## Use standardized naming conventions on components to the best of your ability, ESPECIALLY plywood as these are not automatically counted by the BOM.
### IMPORTANT: There are things added manually like plywood through their names so check these conditionals, see main.py, it is labeled.
### There is a counter program in woodScrewCounter folder that is ran to get estimates for screws and wood, this will use the names in the FASTENERS.csv for screw counting and in main.py there is an override for specific names.
### TODO: The names in the FASTENERS.csv goes through conditioning before checking. I want to remove this conditioning to better add informer code when there is something in the CAD and not the FASTENERS.csv and vice verse

# Rule 5
## Dont use intersecting pieces if you are going to use solidworks for load calculations
## Example
![App Screenshot](images/dontDoThis.png)

## Authors
Marlow Tracy, Zack Ivanisevic, Luke Anger, Kanokwan Tungkitkancharoen

For reference here is the past years
https://gitlab.com/ec-build-2023/fort-2023-cad

